variable "name" {
  type = string
  description = "Name of the RAM share to create"
}

variable "principals" {
  type = list
  description = "List of principals to share with - may be Account IDs, Organization ARNs, or Organization Unit ARNs"
}

variable "arns_to_share" {
  type = list
  description = "List of ARNs for the resources to share"

  validation {
    # Parses out the 'type' field from each ARN and checks to see if that type is supported
    # If any ARN is not of a supported type, validation fails
    condition = !contains([for arn in var.arns_to_share:
                   contains (["prefix-list"], dirname(split(":", arn)[5]))],false)
    error_message = "ARNs to be shared must be of a supported type: prefix-list."
  }


}
