locals {
  supported_arn_types = ["prefix-list"]
}

resource "aws_ram_resource_share" "this" {
  name = var.name
  allow_external_principals = false
}

resource "aws_ram_principal_association" "this" {
  count = length (var.principals)
  principal = var.principals[count.index]
  resource_share_arn = aws_ram_resource_share.this.arn
}

resource "aws_ram_resource_association" "this" {
  count = length (var.arns_to_share)
  resource_arn = var.arns_to_share[count.index]
  resource_share_arn = aws_ram_resource_share.this.arn
}
