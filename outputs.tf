output "resource_share_arn" {
  value = aws_ram_resource_share.this.arn
}

output "shared_resources" {
  value = aws_ram_resource_association.this[*].resource_arn
}
